# coding=utf-8
from multiprocessing.sharedctypes import Value
from flask import Flask, render_template, request, jsonify
import json
import google.cloud.dialogflow_v2 as dialogflow
import os
from sys import platform
from datetime import datetime

"""
Da Python auf Linux einen absoluten Pfad benötigt:
Erkennen des Betriebsystems. Falls Linux, location der gespeicherten Files angeben
"""

if platform == "win32":
    location = ""
elif platform == "linux":
    location = "/var/www/webApp/webApp/"


app = Flask(__name__, template_folder='./templates')

"""
Erstellen der Routen und Rendern der HTML Files.
"""


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/password')
def password():
    return render_template('password.html')


@app.route('/chat')
def chat():
    return render_template('chat.html')


@app.route('/preChat')
def preChat():
    return render_template('preChat.html')


# Gibt den Hash wert für passwortkontrolle zurück
@app.route('/gethash', methods=['GET'])
def gethash():
    return '1540192'

# @app.route('/umfrage')
# def umfrage():
#    return render_template('umfrage.html')


@app.route('/gewinnspiel')
def gewinnspiel():
    return render_template('gewinnspiel.html')

# @app.route('/chatbody')
# def chatbody():
#    return render_template('chatbody.html')


@app.route('/brief')
def brief():
    return render_template('brief.html')


"""
Teilnehmerseite, bei der Abgefragt werden kann, wie viele Menschen bereits Teilgenommen haben.
"""


@app.route('/teilnehmer')
def teilnehmer():
    with open(location + 'storage/counter.json') as f:
        data = json.load(f)
    with open(location + 'storage/time.json') as f:
        time = json.load(f)

    brief = str(data["brief"]) + " haben den Brief gesehen*"
    chat = str(data["chat"]) + " haben mit dem Chatbot geschrieben*"
    zeit = "Fetch von Qualtrics um: " + str(time["time"])
    final = brief+chat+zeit
    final = final.split("*")

    return render_template('teilnehmer.html', mylist=final)

# @app.route('/webhook', methods=['POST'])                    #Webhook für Chatbot. Mal gucken ob wir den benutzen
# def webhook():



"""
Wenn diese Methode nicht antwortet, kann checkifonline.py den Server neustarten
"""
@app.route('/isonline', methods=['GET'])
def isonline():
    return "OK"


"""
Route Über die Ein GET Request ausgeführt werden kann, welcher zurückgibt, ob Chat oder Brief angezeigt werden soll.
Die Jeweilige Komponenten wird hochgezählt und anschließend ausgegeben.

"""

@app.route('/counter', methods=['GET'])
def counter():
    with open(location + 'storage/counter.json') as f:
        data = json.load(f)
    brief = int(data["brief"])
    chat = int(data["chat"])

    if(brief <= chat):
        brief = brief + 1
        data["brief"] = str(brief)
        antwort = {"auswahl": "brief"}
    elif(chat < brief):
        chat = chat+1
        data["chat"] = str(chat)
        antwort = {"auswahl": "chat"}

    with open(location + 'storage/counter.json', 'w') as f:
        json.dump(data, f)
    return jsonify(antwort)

"""
Updatecounter Updated den Counter auf die aktuellen Zahlen aus Qualtrics. Diese werden vom Script
qulatrics.py ausgelesen und per Post gesendet
"""

@app.route('/updatecounter', methods=['POST'])
def updatecounter():
    brief = request.form['brief']
    chat = request.form['chat']
    key = request.form['key']
    if(key == "73hhfddfu3.df.dfj33k4jdfsd3ldks"):  
        timeinstr = datetime.now().strftime("%H:%M:%S , %d/%m/%Y")
        data = {"brief": str(brief), "chat": str(chat)}
        time = {"time": timeinstr}

        with open(location + 'storage/time.json', 'w') as f:
            json.dump(time, f)
        with open(location + 'storage/counter.json', 'w') as f:
            json.dump(data, f)
        return "OK"
    else:
        return "Wrong Key"

"""
send_message startet einen Request an Google Dialogflow, mit einer Nachricht, die es vom Server erhält und gibt Anschließend das Ergebnis an den Client zurück.
"""
@app.route('/send_message', methods=['POST'])
def send_message():
    message = request.form['message']
    sessionid = request.form['sessionid']
    project_id = "test-elaw"
    fulfillment_text = gettext(project_id, str(sessionid), message, 'de')
    antwort_text = {"message":  fulfillment_text}
    antwort_text = jsonify(antwort_text)
    return antwort_text




os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = location + "api_key.json"
client = dialogflow.SessionsClient()

def gettext(project_id, session_id, text, language_code):
    # Methode startet einen Sessionclient session id wird auch übergeben um user zu identifizieren
    session = client.session_path(project_id, session_id)
    if text:
        text_input = dialogflow.types.TextInput(
            text=text, language_code=language_code)
        query_input = dialogflow.types.QueryInput(text=text_input)
        response = client.detect_intent(
            session=session, query_input=query_input)
        return (response.query_result.fulfillment_text)


if __name__ == "__main__":
    app.run()
