const umfrage = "https://wiwigoettingen.eu.qualtrics.com/jfe/form/SV_3ReL5pHdrQQXkns";


//Diese Funktion überprüft zusammen mit den Funktionen hashing und checkhash das eigegeben Passwort.
function checkpassword(){                                           //Diese Funktion holt den Value aus der Eingabe
    eingabefeld = document.getElementById("inputpassword");
    checkhash(eingabefeld);
}

//Überprüft ob Enter gedrückt wird.
function clickPress(event) {
    if (event.keyCode == 13) 
    {
        checkpassword();
    }
}


//Erstellt eine Hashwert mit Bitverschiebung.
function hashing(eingabe){                                          //Diese Funktion erstellt einen hash per bitshift nach links
    console.log(eingabe);
    var hash = 0;
    if (eingabe.length == 0)
    {
        return hash;
    }
    for(i=0; i<eingabe.length;i++)
    {
        char = eingabe.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
    console.log("Hash:" + hash);
    return hash;
}


//Funktion Holt den Hashwert per GET vom Server und Stellt einen Timer nach Erfolgreicher Eingabe zur Verfügung.

function checkhash(eingabefeld){                                        //Diese funtkion holt den Hashwert per get mit ajax vom Server und vergleicht die werte
    var ausgabefeld = document.getElementById("ausgabe");
    var ausgabe= hashing((eingabefeld.value));
    console.log(ausgabefeld)
    $.get("/gethash",function(data)                              //Benutzen der get Funktion von ajax jquery
    {
        if(String(ausgabe) == String(data))
        {
            ausgabefeld.innerHTML = "Passwort ist richtig"
            var timeleft = 5;                                           //Das hier ist ne funktion für einen Countdown
            var timer = setInterval(function()
            {
                if(timeleft <= 0)
                {
                    clearInterval(timer);
                    //window.open("/umfrage","_top");                     //Öffnen der Umfrage
                    window.open(umfrage,"_self");
                } 
                else 
                {
                    ausgabefeld.innerHTML = "Weiterleitung zur Umfrage in: " + timeleft;            //Countdown solange bis runtergezählt von timeleft
                }
                timeleft -= 1;
            }, 1000);

            
        }
        else
        {
            ausgabefeld.innerHTML = "Passwort war falsch"
        }
    })
};



//Diese Methode entscheidet, Welche der Beiden Sachen der User angezeigt bekommt.
//Wird vom Button auf der Indexseite direkt aufgerufen.
function choice(){
    $.get("/counter",function(data)
    {        

        if ("brief" == String(data.auswahl))
        {
            window.open("/brief","_top");
        }
        else if("chat" == String(data.auswahl))
        {
            window.open("/preChat","_top");
        }
    })
}


