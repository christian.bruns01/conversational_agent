const umfrage = "https://wiwigoettingen.eu.qualtrics.com/jfe/form/SV_eFJyMqTEdswVPGS";
const sessionid = String(getrandom(1,999999))


//Erstellt eine Randomisierte Nummer für die SessionID des Google Dialogflow Clients.
function getrandom(min, max) {
    return Math.random() * (max - min) + min;
}



//Sendet die Nachricht aus preparesend() an den Webserver und Erwartet die Antwort.
function send_message(message)                    
{
    $.post("/send_message", { message: message , sessionid: sessionid}, response);
}


//Wenn umfrage eingegeben wurde, wird man an die Umfrage weitergeleitet. Ansonsten wird die Nachricht von Dialogflow ausgegeben.
function response(data)                      
{
    // append the bot repsonse to the div
    if(data.message == "umfrage"){
        //window.open('/umfrage',"_top")
        window.open(umfrage,"_self");
    }
    $('#chat').append(`
            <div class="chat-message bot-message">
                ${data.message}
            </div>

      `)
    // remove the loading indicator
    $("#loading").remove();
    scrollen();
};


//Holt die Input Message und gibt diese an send message weiter. Erstellt ausßerdem ein div, in dem dei Message vom Mensch ist und ein leeres div für die Antwort vom Server
function preparesend()
{
    const input_message = $('#input_message').val()
    // return if the user does not enter any text
    if (!input_message) 
    {
        return;
    }

    $('#chat').append(`
        <div class="chat-message col-md-5 human-message">
            ${input_message}
        </div>
    `);

    // loading 
    $('#chat').append(`
        <div class="chat-message bot-message" id="loading">
            <b>...</b>
        </div>
    `);

    // Lösche das Input Feld 
    $('#input_message').val('');
    scrollen();

    // Sende die Nachricht
    send_message(input_message);
};


//Überprüft ob Enter gedrückt wird.
function clickPress(event) {
    if (event.keyCode == 13) 
    {
        preparesend();
    };
};


//Wenn die Seite gerezised wird, dann wird  die Funktion scrollen aufgerufen.
window.addEventListener("resize", scrollen());

//Scrollt den Chat ganz nach Unten.
function scrollen(){
    containerchat = document.getElementById("chatcontainer");
    containerchat.scrollTop = containerchat.scrollHeight;  
};

