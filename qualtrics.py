import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import requests
def main():
    options = Options()
    options.add_argument("--headless")
    driver = uc.Chrome(options=options)
    driver.get('https://login.qualtrics.com/login?lang=DE')

    # Auf der Webseite anmelden
    WebDriverWait(driver,30).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="loginButton"]'))
    )
    driver.find_element(By.XPATH, '//*[@id="UserName"]').send_keys('')
    driver.find_element(By.XPATH, '//*[@id="UserPassword"]').send_keys('')
    driver.find_element(By.XPATH, '//*[@id="loginButton"]').click()
    
    #Finden der Elemente und Daten extrahieren
    WebDriverWait(driver,30).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="tile-resource-SV_eFJyMqTEdswVPGS"]'))
    )
    chat = driver.find_element(By.XPATH, '//*[@id="tile-resource-SV_eFJyMqTEdswVPGS"]/div/div[4]/span').get_attribute('innerText')
    mail = driver.find_element(By.XPATH, '//*[@id="tile-resource-SV_3ReL5pHdrQQXkns"]/div/div[4]/span').get_attribute('innerText')
    print(chat)
    print(mail)

    #Daten in JSON-Format convertieren 
    data = {"brief": str(mail), "chat": str(chat), "key": "73hhfddfu3.df.dfj33k4jdfsd3ldks"}


    #Daten an den Server senden
    url = "https://v21216.1blu.de/updatecounter"
    x = requests.post(url, data=data)
    print(x.text)


if __name__ == '__main__':
    main()