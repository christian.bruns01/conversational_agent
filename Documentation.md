﻿# Documentation

In der Dokumentation gehe ich auf die einzelnen Methoden die Verwendet werden ein. HTML und CSS Code wird nicht erklärt. 
Der Code befindet sich in den JS und den Python Dateien. 
Die Abschnitte richten sich nach den Dateinamen.

- init.py
- app.js
- chat.js

## *__init__.py*

### teilnehmer()

Die Funktion ließt die Anzahl der Teilnehmer aus der counter.json aus, sowie die Zeit des letzten Qualtrics Fetches aus der time.json
Rendert zusammen mit diesen ausgelesenen Daten eine html, auf der diese ausgegeben werden.


### isonline()
Returned ein "OK". Dient dem Script _checkifonline.py zu überprüfen, ob die Webseite noch requests entgegen nimmt.

### counter()
Lädt die counter.json, ließt die  Werte aus und returned, welcher von beiden weniger angezeigt wurde. Außerdem wird das jeweilige Element hochgezählt und das Ergebnis wieder in der json abgelegt. 

Der Client führt über die app.js diese Funktion per GET aus. Sie dient dem Zweck, die Betrachter des Chatbots und des Briefes gleichzuverteilen.

### updatecounter()

Die Funktion updatecounter erhält per POST die aktuelle Anzahl an Teilnehmern der Umfrage aus Qualtrics. Diese werden dann in der counter.json abgelegt. 
Zweck ist es, dass trotz Abbrechern Brief und Chatbot weiterhin gleichverteilt angezeigt werden.

### send_message()
Erhält per POST die Nachricht und eine Session ID vom Client. sendet diese an gettext() und gibt das Ergebnis von gettext() an den Client zurück.

Diese Methode handled zusammen mit gettext() das senden der vom Nutzer eingegeben Nachrichten von und zu Dialogflow. 

### gettext()
Erstellt eine Session und ein query  für Dialogflow.
Sendet die Nachricht des Nutzers an Dialogflow,  
fängt dann die Antwort Nachricht ab und gibt die Message in der Nachricht an die Methode send_message() zurück.

## *app.js*
Die Funktionen in der app.js haben zwei Aufgaben. Einmal Gleichverteilung auf Brief und Chat, sowie überprüfen des Passwortes, welches man am Ende vom Brief bekommt.

### clickpress()
Wenn Enter gedrück wird, ruft diese Methode checkpassword() auf.

### checkpassword()
Holt den Wert aus der Passworteingabe und gibt diesen an checkhash() weiter

### checkhash()
Holt sich das ausgabefeld sowie den gehashten Wert des Eingabefeldes für das Passwort.
Das Hashing läuft über die Methode hashing().

Anschließend holt er per GET den Vergleichswert vom Server ("/gethash) 

- Passwort richtig --> Countdown von 5, dann Weiterleitung zur Umfrage
- Passwort falsch --> Ausgabe, dass Passwort falsch.

### hashing()
Erstellt einen Hashwert über eine Bitverschiebung.
Jedes Char im Eingabestring, wird in dessen Char Code umgewandelt. 

Der Hash funktioniert mit einem Bitshift. 
In einer For Schleifen werden an den Bitwert des jeweiligen Eingabechars fünf Nullen angehangen und minus des alten Hashwertes gerechnet. Anschließend wird der Wert des Chars wieder draufgerechnet. 
Der temporäre Hash wird anschließend mit sich selbst "and" genommen. Um einen 32Bit Integer zu erzwingen.

### choice()
Die Methode choice() holt sich per GET vom Server, ob Brief oder Chat angezeigt werden soll.

Request läuft über jquery und erhält einen String.

## *chat.js*

### getrandom()
Erstellt eine randomisierte Session ID, Diese wird später an Dialogflow übergeben, um die Nutzer auseinanderzuhalten.

### preparesend()
Holt sich die Eingabe und prüft ob diese nicht Leer ist. Dann hängt er die Eingabe an das Chat Div und fügt eine zweite Botnachricht mit "..." hinzu, die als Platzhalter dient, bis der Bot antwortet.
Außerdem löscht er die inputmessage.
Dann wird die methode send_message() aufgerufen

### send_message()
send_message() macht einen POST Request an den Server. Dieser enthält die Eingegebene Nachricht und die Session ID. Mit der Antwort wird response() aufgerufen.

### response()

In response() wird überprüft ob die Antwortnachricht nur aus "umfrage" besteht. Dann wird der Nutzer zur Umfrageseite weitergeleitet.

 Ansonsten wird die Nachricht an das Chatfenster angehängt und die Nachricht mit den 3 Punkten wird gelöscht.

### clickpress()
Überwacht ob der Nutzer Enter drückt. Wenn ja, wird die Methode preparesend() ausgeführt.

### scrollen()
Die Methode Scrollen scrollt das Chatfenster ganz nach unten. Wird mehrmals im Code aufgerufen, vorallem nach dem Senden von Nachrichten, oder wenn die Seite gerezised wird.




