# Chatbot
## Description
Dieses Anwendung gehört zu einem Chatbot Experiment. Sie besteht aus einem Webserver, der Python und Flask als Webframework nutzt, sowie einer WebApp, die einen Chatbot/Conversational Agent mithilfe von Google Dialogflow implementiert.

## Prerequisites
- Python 3.8
	- Flask
	- json
	- os
	- google.cloud.dialogflow_v2
	- datetime
	- sys
	- undetected_chromedriver
	- selenium_webdriver
- Javascript
	- bootstrap.min.js
	- jquery.min.js

## Installation
### Installationsanleitung für Ubuntu 20.02


#### Dokumentation kann zum Zeitpunkt der Installation veraltet sein. 

### Erstellen des Dialogflow API Keys

1. Google Cloud Platform öffnen
2. Projektname des Bots auswählen (In der Taskleiste)
3. **Notieren der ProjektID**
4. Dann auf APIs & Services --> Anmeldedaten
5. Dann oben **Anmeldedaten erstellen** --> Dienstkonto
6. Individuell ausfüllen außer bei Punkt 2 dort --> Rolle --> Dialogflow API-Client --> weiter bis fertig
7. Anschließend das erstellte Dienstkonto öffnen --> Schlüssel --> Schlüssel hinzufügen --> JSON --> herunterladen

Die JSON wird benötigt um den Chat mit Dialogflow zu verbinden. Die Dateien müssen im selben Ordner liegen wie die restlichen Server Dateien. 

- In der .env Anpassen der Projekt ID.
- Die JSON in den selben Ordner. Name:  **api_key.json**
___

### Installation für den WebServer
Webserver Updaten

    sudo apt update

Webserver Upgraden

    sudo apt upgrade

Apache2 und Nano installieren

    sudo apt install apache2
    sudo apt install nano

Firewall konfigurieren

    sudo apt install ufw
    sudo ufw allow 'Apache'
    sudo ufw allow 'ssh'

Installieren von WSGI und Python für Apache2

    sudo apt install libapache2-mod-wsgi-py3 python-dev
    sudo apt install python3-pip
    pip install flask json datetime untedetected-chromedriver google-cloud-dialogflow selenium

Nun müssen mehrere Ordner erstellt werden

    cd /var/www
Dort:

    mkdir webApp
dann

    sudo nano /etc/apache2/site-available/webApp.conf

In diese Datei wird folgendes geschrieben

    <VirtualHost *:80>
		ServerName hier_ip_vom_server_eintragen
		ServerAdmin email@meinewebseite.de
		WSGIScriptAlias / /var/www/webApp/webapp.wsgi
		<Directory /var/www/webApp/webApp/>
			Order allow,deny
			Allow from all
		</Directory>
		Alias /static /var/www/webApp/webApp/static
		<Directory /var/www/webApp/webApp/static/>
			Order allow,deny
			Allow from all
		</Directory>
		ErrorLog ${APACHE_LOG_DIR}/error.log
		LogLevel warn
		CustomLog ${APACHE_LOG_DIR}/access.log combined
	</VirtualHost>

Abschließend die Datei abspeichern

Jetzt wird ein wsgi file in dem /var/www/webApp Ordner erstellt.

    cd /var/www/webApp
    sudo nano webapp.wsgi

Diesen Code reinkopieren:

    #!/usr/bin/python 
    import sys 
    import logging 
    logging.basicConfig(stream=sys.stderr) 
    sys.path.insert(0,"/var/www/webApp/")
     
    from webApp import app as application 
    application.secret_key = 'Hier einen Random Secret Key'

Erstellen des Ordners Webapp in /var/www/webApp

    mkdir webApp
   
   In diesen Ordner werden per WinSCP alle Server dateien übertragen
   ![Foto zeigt den finalen Webserver Ordner](https://gitlab.gwdg.de/christian.bruns01/conversational_agent/-/raw/main/FOTOS_README/Webserver_Picture1.png)

Anschließend sicherstellen, dass der komplette Ordner www diese Rechte besitzt.

![Foto Zeigt die korrekte Rechtevergabe](https://gitlab.gwdg.de/christian.bruns01/conversational_agent/-/raw/main/FOTOS_README/rechte_www.png)

Enablen der Seite in Apache2

    sudo a2ensite webApp
Apache2 restarten

    systemctl restart apache2

Installieren von Certbot und installationsschritte durchgehen.
Wichtig redirect erzwingen, der gesamte Traffic soll über https laufen.

    sudo apt install certbot python3-certbot-apache
    certbot

### Events anpassen

Es kann vorkommen, dass Apache abstürtzt wenn parallel zu viele Verbindungen offen sind, daher passen wir die mpm_events an.
[mpm_event](https://httpd.apache.org/docs/2.4/mod/event.html)

    nano /etc/apache2/mod-available/mpm_event.conf
- Hier Max Request Workers auf hohe Zahl: z.B. 8000
- Max Connections per Child auf hohe Zahl z.B. 99999

Die Einstellungen sind geeignet um ein crashen während der Kommunikation mit dem Chatbot zu verhindern. Keine Garantie für Richtigkeit.


### Installation für die Abfrage bei Qualtrics

Das Script qualtrics.py wird in einem beliebigen Ordner abgelegt. 

Dann Vergabe der Ausführungsrechte:

    chmod +x qualtrics.py

Installieren von Google Chrome

    wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    
    sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
    
    sudo apt update
    
    sudo apt install google-chrome-stable

Anschließend wird der Cronjob erstellt.

    crontab -e

Beispiel:  Script wird alle  15 Minuten ausgeführt.

    */15 * * * * /usr/bin/python3 /root/scripts/qualtrics.py >/dev/null 2>&1

## License
[Creative Commons Attribution-NonCommercial](https://creativecommons.org/licenses/by-nc/4.0/)
