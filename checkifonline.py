import requests
import subprocess

def main():
    url = "https://v21216.1blu.de/isonline"
    try:
        x = requests.get(url,timeout=5)
        if x.text == "OK":
            return
        else:
            subprocess.Popen("systemctl restart apache2", shell=True)
            return
    except:
        subprocess.Popen("systemctl restart apache2", shell=True)
        return

if __name__ == '__main__':
    main()